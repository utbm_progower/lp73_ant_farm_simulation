# LP73 Ant Farm Simulation : Rapport

![Icon](../icon.png)

## Table Of Contents

- [LP73 Ant Farm Simulation : Rapport](#lp73-ant-farm-simulation--rapport)
  - [Table Of Contents](#table-of-contents)
  - [Description](#description)
  - [Equipe](#equipe)
  - [Organisation](#organisation)
  - [Support](#support)
  - [Analyse](#analyse)
    - [Décomposition des objets](#décomposition-des-objets)
    - [Diagramme de classe : Librairie](#diagramme-de-classe--librairie)
    - [Diagramme de classe : GUI](#diagramme-de-classe--gui)
  - [Technologies](#technologies)
  - [Intelligence du système](#intelligence-du-système)
  - [Evolutions possibles](#evolutions-possibles)
  - [Conclusion](#conclusion)

## Description

Ce projet est un logiciel de simulation du comportement des fourmis dans un environnement contenant des obstacles et des sources de nourriture. L'objectif est de créer un simulateur avec des paramètres dynamiques afin de réaliser diverses simulations sur les fourmis.

## Equipe

- Léo Debuigny
- Thomas Margueritat

## Organisation

Pour commencer, nous avons choisi de travailler selon la méthode du Pair Programming, l'objectif étant de travailler plus vite tout en ayant une connaissance de l'ensemble de l'application sans se marcher sur les pieds.

A l'aide de divers outils actuels, nous avons pu travailler ensemble sur la même machine en coopération. Ainsi, il était plus simple de travailler ensemble sur le projet.

Le programme étant écrit en C++, il nécessite une certaine rigueur dans l'écriture du code, nous avons donc choisit de vérifier à tour de rôle notre code afin de ne pas avoir à revenir dessus par la suite.

L'utilisation d'un repository Git est devenue une obligation pour organiser correctement ce projet. Ainsi, avec l'aide de GitLab, nous avons crée un repository Git pour accueillir l'ensemble de notre code source, une organisation en tableau Kanban et toutes nos documentations.

Pour finir, nous avons choisi comment organiser le projet. Nous avons choisi d'organiser le projet en 4 parties :

- **Ant Farm Library** : Librairie qui contient toute l'intelligence et les classes principales de notre projet. Cela permet de l'importer par la suite dans n'importe quel projet pour l'exploiter.
- **Unit Test** : Pour obtenir la meilleure stabilité possible pour les fondations de notre programme, nous avons décidés de couvrir le code nos classes de tests unitaires pour vérifier leur fonctionnement et le système d'héritage et éviter les regressions.
- **Console** : Par la suite, pour exploiter la librairie et lancer des simulations, nous avons choisi un affichage en mode console pour commencer et tester nos résultats.
- **GUI** : Pour finir, une fois que nous avons obtenu un résultat satisfaisant pour la librairie, nous avons décidé de mettre au point une interface graphique complète pour lancer proprement des simulations avec notre librairie.

## Support

- ![Subject](./supports/lp73-projet-2020print.pdf)

## Analyse

Une fois la base choisit, il fallait maintenant analyser le sujet au maximum et décomposer tous les éléments nécessaires.

### Décomposition des objets

![System](./img/system.png)

Après cette visualisation graphique du travail final à obtenir, nous avons commencé à réfléchir sur la manière dont nous voulions organiser notre librairie.

Nous avons donc mis au point ce diagramme de classe après beaucoup de réflexion et de changement, l'objectif était d'avoir prévu le maximum avant de commencer à programmer, car une fois la programmation lancée, il est difficile de faire des changements à la racine du projet et on risque d'accumuler de la dette technique.

### Diagramme de classe : Librairie

![Library Class Diagram](./img/class_diagram.png)

### Diagramme de classe : GUI

![GUI Class Diagram](./img/gui_class_diagram.png)

Nous avons donc une grande quantité de classe avec de l'héritage entre elles.

Il nous fallait 2 informations cruciales pour la majorité de nos objets : les coordonnées de celui-ci sur une carte à 2 dimensions et le type d'objet qu'il représente. Pour palier à ce problème, nous avons créé deux classes mères principales :

- **Coord** : Permet de gérer les coordonnées de l'objet en question.
- **Element** : Permet de gérer le type de l'objet en question.
  - **Map** : Permet de gérer une carte à 2 dimensions d'objet de type **Element**.

Par la suite, divers objets héritent de **Element** :

- **Obstacle** : Permet de gérer des obstacles.
- **Food** : Permet de gérer les sources de nourriture.
- **Pheromone** : Permet de gérer un type de phéromone.
  - **PheromonePack** : Permet de gérer deux types de phéromone (Retour à la maison et recherche de nourriture).
    - **MapPheromone** : Permet de gérer une carte à 2 dimensions de **PheromonePack**.
- **Ant** : Permet de gérer la structure de base d'une fourmi
  - **GrowingAnt** : Permet de gérer les fourmis de type "oeufs" et "larves".
  - **QueenAnt** : Permet de gérer la reine des fourmis.
  - **WorkerAnt** : Permet de gérer les fourmis ouvrières.
  - **WarriorAnt** : Permet de gérer les fourmis guerrières.
  - **AntFarm** : Permet de gérer la fourmilière.

Pour finir, nous avons le système de simulation divisé en deux classes :

- **Config** : Permet de configurer facilement le simulateur.
- **MainProgram** : Contient l'ensemble de l'intelligence pour lancer des simulations.

## Technologies

Nous avons utilisé diverses technologies pour travailler sur ce projet :

- **Git** : Gestionnaire de version pour le code source du projet.
- **GitLab** : Hébergeur du code source et de la documentation du projet.
- **GitLab CI** : Intégration continue pour le projet.
- **Docsify** : Générateur de documentation en version web.
- **Doxygen** : Générateur de documentation technique pour le code source en C++.
- **Docker** : Moteur de containerisation pour gérer la construction de notre programme.
- **CPPLint** : Linter pour vérifier la qualimétrie du code source.
- **Qt** : Librairie pour concevoir des interfaces graphiques.
- **Qt Creator et VS Code** : IDE complets pour gérer le code source et la réalisation de l'interface graphique.

Le mélange de ces technologies est difficile à prendre en main, mais cela nous a permis d'avoir un travail très propre du début à la fin avec beaucoup de simplification dans certaines tâches.

## Intelligence du système

Pour commencer, nous avons choisi un système d'itération pour gérer chaque modification sur la simulation, ainsi l'utilisation doit lancer une ou plusieurs itérations pour que des changements s'effectuent sur la simulation. Une fonction principale "iterate" permet d'effectuer une itération lorsque le simulateur est configuré.

Ensuite, nous utilisons deux types de phéromones pour que la fourmi guerrière puisse aller chercher de la nourriture et la ramener à la fourmilière :

- **Phéromone de "recherche de nourriture"** : La fourmi guerrière recherche de la nourriture et dépose des phéromones de type "recherche de nourriture" lorsqu'elle trouve une source de nourriture.
- **Phéromone de "retour à la maison"** : Une fois chargée de nourriture au maximum, elle cherche à retourner à la fourmilière, elle dépose des phéromones de type "retour à la maison" lorsqu'elle cherche de la nourriture et également lorsqu'elle rentre à la maison. Si pendant sa route, elle mange toute la nourriture stockée, alors elle repasse en mode recherche de nourriture.

La fourmi guerrière dispose ainsi de deux modes de fonctionnement, le mode où elle recherche de la nourriture et celui pour rentrer à la fourmilière.

La fourmi guerrière prendra des décisions par rapport à ce qui l'entoure principalement ainsi que son état actuel, par exemple, une fourmi guerrière affamée va rechercher de la nourriture en priorité. Identique pour la reine, elle pondra des oeufs uniquement si c'est possible.

## Evolutions possibles

- **Librairie** :
  - Améliorer le système de prise de décision.
  - Augmenter la quantité de variables paramétrables.
  - Intégrer la gestion multi fourmilières.
- **Interface Console** :
  - Améliorer l'affichage des informations.
  - Ajouter la possibilité de saisir directement les paramètres ou de les passer en ligne de commande.
- **Interface Graphique GUI** :
  - Ajouter un système de sauvegarde des simulations.
  - Ajouter un système d'historique des itérations.
  - Ajouter plus de paramètres disponibles.
  - Ajouter un mode auto pour faire défiler les itérations à intervalle de temps régulier.

## Conclusion

Pour conclure, ce projet nous a beaucoup apportés en autonomie et gestion de projet, nous avons évolué dans la mise en place de notre organisation pour travailler. Le C++ est un langage puissant et structuré, il est agréable à utiliser pour de l'orienter objet et ce projet nous a permis de bien mieux appréhender le C++. Pour finir, la mise en place d'une interface graphique complète avec des technologies inconnues était un défi, mais nous sommes très satisfait du résultat et de ce que nous avons appris avec ce projet.
