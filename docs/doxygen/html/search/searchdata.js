var indexSectionsWithContent =
{
  0: "_abcdefgilmnopqrsuw~",
  1: "acefgmopqw",
  2: "u",
  3: "acefgmopquw",
  4: "abcdefgimopqrsuw~",
  5: "_afilmnpw",
  6: "aq"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Macros"
};

