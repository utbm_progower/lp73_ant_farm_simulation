var searchData=
[
  ['nb_5feggs_5fmax_5fper_5fiteration',['nb_eggs_max_per_iteration',['../class_config.html#a7a5312e6067250d4be24e39f208efb97',1,'Config']]],
  ['nb_5ffood_5fant_5fcreation',['nb_food_ant_creation',['../class_config.html#a3c2d16d06a4889ca7dd07693047d47ee',1,'Config']]],
  ['nb_5fiterate',['NB_ITERATE',['../console_2main_8cpp.html#a817ce84ad45a014c565cbffbf8b17fbf',1,'main.cpp']]],
  ['nb_5fobstacle',['nb_obstacle',['../class_config.html#a758b794f5b136512336ac1a32dfc77c7',1,'Config']]],
  ['nb_5fsource_5ffood',['nb_source_food',['../class_config.html#ac1e29b0e76241cecdcacdf0bebc8d546',1,'Config']]],
  ['nb_5fwarrior_5fant_5finit',['nb_warrior_ant_init',['../class_config.html#a81500b5ee0f57fa6d7caf4a2c892ba48',1,'Config']]],
  ['nb_5fworker_5fant_5finit',['nb_worker_ant_init',['../class_config.html#a84755f922d195e4812e42f2ddd29cc41',1,'Config']]]
];
