# LP73 Ant Farm Simulation : Idea

![Icon](../icon.png)

## Table Of Contents

- [LP73 Ant Farm Simulation : Idea](#lp73-ant-farm-simulation--idea)
  - [Table Of Contents](#table-of-contents)
  - [Upgrade Idea](#upgrade-idea)
  - [New Function Idea](#new-function-idea)
  - [Optimize Idea](#optimize-idea)

## Upgrade Idea

- Nothing.

## New Function Idea

- Nothing.

## Optimize Idea

- Nothing.
