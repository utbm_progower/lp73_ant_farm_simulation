# LP73 Ant Farm Simulation : Conception

![Icon](../icon.png)

## Table Of Contents

- [LP73 Ant Farm Simulation : Conception](#lp73-ant-farm-simulation--conception)
  - [Table Of Contents](#table-of-contents)
  - [Documentation](#documentation)
  - [System](#system)
  - [Main Class Diagram](#main-class-diagram)
  - [GUI Class Diagram](#gui-class-diagram)

## Documentation

- ![Subject](./supports/lp73-projet-2020print.pdf)

## System

![System](./img/system.png)

## Main Class Diagram

![Class Diagram](./img/class_diagram.png)

## GUI Class Diagram

![Class Diagram](./img/gui_class_diagram.png)
