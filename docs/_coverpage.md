![icon](./icon.png)

# LP73 Ant Farm Simulation

> **LP73 Ant Farm Simulation** is a C++ simulator for how many time can survive ant in specific space (By Léo Debuigny and Thomas Margueritat).

[Get Started](#getting-started)
[GitLab](https://gitlab.com/utbm_progower/lp73_ant_farm_simulation)
