# LP73 Ant Farm Simulation

![Icon](./icon.png)

## Table Of Contents

- [LP73 Ant Farm Simulation](#lp73-ant-farm-simulation)
  - [Table Of Contents](#table-of-contents)
  - [Description](#description)
  - [Access](#access)
  - [Teams](#teams)
  - [Documentation](#documentation)
  - [Software Paramters](#software-paramters)
  - [Build](#build)
    - [Build : QMake](#build--qmake)
    - [Build : Docker](#build--docker)
  - [Launch : Console](#launch--console)
  - [Check Code Synthax](#check-code-synthax)
  - [Docsify Build and Deploy](#docsify-build-and-deploy)
  - [Doxygen](#doxygen)
  - [Licence](#licence)

## Description

LP73 Ant Farm Simulation is a C++ simulator for how many time can survive ant in specific space. Here the ![subject](./docs/supports/lp73-projet-2020print.pdf).

## Access

- [Global Documentation](https://utbm_progower.gitlab.io/lp73_ant_farm_simulation/#/)

## Teams

- Léo Debuigny
- Thomas Margueritat

## Documentation

- [Conception](./docs/conception.md)
- [Report](./docs/report.md)
- [Task](./docs/task.md)
- [Idea](./docs/idea.md)
- [Source Code Documentation](./docs/doxygen/html/index.html)

[Source Code Documentation](./docs/doxygen/html/index.html ':include')

## Software Paramters

| Parameters | Example | Description |
|-|-|-|
| MAP_HEIGHT | 50 | Height of the map |
| MAP_WIDTH | 100 | Width of the map |
| NB_OBSTACLE | 1000 | Number of Obstacles |
| NB_SOURCE_FOOD | 500 | Number of source of Foods |
| PHEROMONE_EVAPORATION_RATE | 95 | Pheromone evaporation rate for each iteration |
| ANT_MAX_LIFE | 100 | Max Life (PV) of Ant |
| ANT_MAX_AGE | 1000 | Max Age of Ant (when age = 1000, ant died), must be ANT_MAX_AGE > ANT_MAX_LIFE |
| ANT_FARM_MAX_POPULATION | 100 | Max amount of ant in the Ant Farm |
| ANT_FARM_MAX_FOOD | 10000 | Max amount of food in the Ant Farm |
| ANT_QUEEN_FOOD_CONSUMPTION | 3 | Amount of Food ate by queen ant for each iteration |
| ANT_FOOD_CONSUMPTION | 1 | Amount of Food ate by ant for each iteration |
| NB_FOOD_ANT_CREATION | 10 | Amount of Food for create an Ant |
| NB_EGGS_MAX_PER_ITERATION | 3 | Number max of eggs created per iteration |
| INCUBATION_TIME | 10 | Number of iteration for a eggs to be a larva |
| LARVAL_STAGE_DURATION | 10 | Duration of larval stage |
| WORKER_STAGE_DURATION | 300 | Duration of worker stage |
| NB_WORKER_ANT_INIT | 10 | Number of worker ant at start |
| NB_WARRIOR_ANT_INIT | 30 | Number of warrior ant at start |
| FOOD_SOURCE_MAX_QUANTITY | 1000 | Number of maximal food source |
| WARRIOR_MAX_FOOD | 50 | Amount of food for warrior |
|  |  |  |

## Build

### Build : QMake

    cd src
    qmake
    make

### Build : Docker

    docker-compose up

## Launch : Console

    cd ./src/console
    export LD_LIBRARY_PATH=../ant_farm_library && ./console

    # With GUI (Required Qt 5.12, the best is to launch it with Qt Creator)
    cd ./src/gui
    export LD_LIBRARY_PATH=../ant_farm_library && ./gui

## Check Code Synthax

    cd src

    # Setup Dependancies
    pipenv install

    # Check Code Synthax
    pipenv run cpplint --recursive *

## Docsify Build and Deploy

    cd docsify
    
    # Build
    docker-compose build

    # Deploy
    docker-compose up -d

You can access in local to your generated [Documentation](http://localhost:6007)

## Doxygen

Doxygen is a tool for generate source code documentation.

    # Install
    sudo apt install doxygen-gui

    # Generate with GUI
    doxywizard

## Licence

This project is licensed under the terms of the GNU General Public Licence v3.0 and above licence.
