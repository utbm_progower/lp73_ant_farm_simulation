#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QVector>
#include <QPushButton>
#include <QLabel>
#include <QListWidget>
#include <QFileInfo>

#include "mainprogram/mainprogram.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void refreshMap();
    void refreshInfo();

public slots:

    void buttonInit();
    void buttonIterate();

private:

    Ui::MainWindow *ui;

    QVector<QVector<QLabel*>*> *_element;
    QLabel *_info;
    QListWidget *_food;
    QListWidget *_warrior;
    QListWidget *_worker;
    QListWidget *_growing;

    Config *_config;
    MainProgram *_mainProgram;

    int _nb_iteration;
};
#endif // MAINWINDOW_H
