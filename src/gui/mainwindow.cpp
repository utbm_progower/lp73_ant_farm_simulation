#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("Ant Farm Simulator");
    this->setWindowIcon(QIcon("./icon.png"));
    this->ui->iterate->setEnabled(false);

    // Setup Basic Config
    this->_config = new Config();
    this->ui->width->setValue(this->_config->map_width);
    this->ui->height->setValue(this->_config->map_height);
    this->ui->food->setValue(this->_config->nb_source_food);
    this->ui->obstacle->setValue(this->_config->nb_obstacle);
    this->ui->pheromone->setValue(this->_config->pheromone_evaporation_rate);
    this->ui->ant_max_life->setValue(this->_config->ant_max_life);
    this->ui->ant_max_age->setValue(this->_config->ant_max_age);
    this->ui->ant_farm_max_population->setValue(this->_config->ant_farm_max_population);
    this->ui->ant_farm_max_food->setValue(this->_config->ant_farm_max_food);
    this->ui->nb_worker_ant_init->setValue(this->_config->nb_worker_ant_init);
    this->ui->nb_warrior_ant_init->setValue(this->_config->nb_warrior_ant_init);
    this->ui->warrior_max_food->setValue(this->_config->warrior_max_food);

    // Create Info Tab
    this->_info = new QLabel("Informations :\n");
    this->_info->setAlignment(Qt::AlignTop);
    this->ui->data_tab->addTab(this->_info, "Info");

    // Create Food Tab
    this->_food = new QListWidget();
    this->ui->data_tab->addTab(this->_food, "Food");

    // Create Warrior Ant Tab
    this->_warrior = new QListWidget();
    this->ui->data_tab->addTab(this->_warrior, "Warrior Ant");

    // Create Worker Ant Tab
    this->_worker = new QListWidget();
    this->ui->data_tab->addTab(this->_worker, "Worker Ant");

    // Create Growing Ant Tab
    this->_growing = new QListWidget();
    this->ui->data_tab->addTab(this->_growing, "Growing Ant");

    QObject::connect(this->ui->init, SIGNAL(clicked(bool)), this, SLOT(buttonInit()));
    QObject::connect(this->ui->iterate, SIGNAL(clicked(bool)), this, SLOT(buttonIterate()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete this->_element;
    delete this->_config;
    delete this->_mainProgram;
}


void MainWindow::refreshMap() {
    Map map = this->_mainProgram->getMap();

    QString img_size = "8px";

    if (this->ui->img_size->isChecked())
        img_size = "16px";

    bool useImg = true;

    if (!QFileInfo::exists("./img/" + img_size + "/ant_farm.png"))
        useImg = false;
    if (!QFileInfo::exists("./img/" + img_size + "/ant.png"))
        useImg = false;
    if (!QFileInfo::exists("./img/" + img_size + "/food.png"))
        useImg = false;
    if (!QFileInfo::exists("./img/" + img_size + "/obstacle.png"))
        useImg = false;

    QPixmap antFarm = QPixmap("./img/" + img_size + "/ant_farm.png");
    QPixmap ant = QPixmap("./img/" + img_size + "/ant.png");
    QPixmap food = QPixmap("./img/" + img_size + "/food.png");
    QPixmap obstacle = QPixmap("./img/" + img_size + "/obstacle.png");

    for (int i = 0; i < this->_config->map_height; i++) {
        for (int j = 0; j < this->_config->map_width; j++) {
            Element element = map.getElement(Coord(i, j));
            QString symbol = QString::fromStdString(element.getTypeSymbol());
            QString coord = "(" + QString::number(element.getX()) + ", " + QString::number(element.getY()) + ")";
            this->_element->at(i)->at(j)->setText(symbol);
            this->_element->at(i)->at(j)->setToolTip(coord);

            if (useImg) {
                if (symbol == "H")
                    this->_element->at(i)->at(j)->setPixmap(antFarm);
                if (symbol == "W")
                    this->_element->at(i)->at(j)->setPixmap(ant);
                if (symbol == "0")
                    this->_element->at(i)->at(j)->setPixmap(food);
                if (symbol == "#")
                    this->_element->at(i)->at(j)->setPixmap(obstacle);
                if (symbol == "")
                    this->_element->at(i)->at(j)->setPixmap(QPixmap());
            }

            int pheromoneQuantity = this->_mainProgram->getMapPheromone().getPheromonePack(element.getCoord()).getResearch().getQuantity();
            if (pheromoneQuantity > 0) {
                QPalette palette = this->_element->at(i)->at(j)->palette();
                if (pheromoneQuantity > 0)
                    palette.setColor(QPalette::Window, QColor(0, 150, 0, 50));
                if (pheromoneQuantity > 100)
                    palette.setColor(QPalette::Window, QColor(0, 150, 0, 100));
                if (pheromoneQuantity > 200)
                    palette.setColor(QPalette::Window, QColor(0, 150, 0, 150));
                if (pheromoneQuantity > 500)
                    palette.setColor(QPalette::Window, QColor(0, 150, 0, 200));
                if (pheromoneQuantity > 1000)
                    palette.setColor(QPalette::Window, QColor(0, 150, 0, 255));
                this->_element->at(i)->at(j)->setPalette(palette);
                this->_element->at(i)->at(j)->setAutoFillBackground(true);
            } else
                this->_element->at(i)->at(j)->setAutoFillBackground(false);
        }
    }
}

void MainWindow::refreshInfo() {
    AntFarm *antFarm = &this->_mainProgram->getAntFarm();
    QString info = "Informations :\n";
    info += "Map Size : (" + QString::number(antFarm->getX()) + ", " + QString::number(antFarm->getY()) + ") : " + QString::number(this->_config->map_height) + " x " + QString::number(this->_config->map_width) + "\n";
    info += "Ant Farm :\n";
    info += "Food Quantity : " + QString::number(antFarm->getFoodQuantity()) + " / " + QString::number(antFarm->getMaxFoodQuantity()) + "\n";
    info += "Population : " + QString::number(antFarm->getPopulation()) + " / " + QString::number(antFarm->getMaxPopulation()) + "\n";
    info += "Queen : Life = " + QString::number(antFarm->getQueenAnt().getLife()) + " / " + QString::number(antFarm->getQueenAnt().getMaxLife());
    info += " | Age = " + QString::number(antFarm->getQueenAnt().getAge()) + " / " + QString::number(antFarm->getQueenAnt().getMaxAge()) + "\n";
    this->_info->setText(info);

    vector<Food> *foodList = &this->_mainProgram->getFoodList();
    this->_food->clear();
    this->_food->addItem("Nb of Food : " + QString::number(foodList->size()));
    for (int i = 0; i < int(foodList->size()); i++)
        this->_food->addItem("(" + QString::number(foodList->at(i).getX()) + ", " + QString::number(foodList->at(i).getY()) + ") : " + QString::number(foodList->at(i).getQuantity()));

    vector<WarriorAnt> *warriorList = &this->_mainProgram->getWarriorAntList();
    this->_warrior->clear();
    this->_warrior->addItem("Nb of Warrior Ant : " + QString::number(warriorList->size()));
    for (int i = 0; i < int(warriorList->size()); i++) {
        this->_warrior->addItem("(" +
                                QString::number(warriorList->at(i).getX()) + ", " +
                                QString::number(warriorList->at(i).getY()) + ") : " +
                                QString::number(warriorList->at(i).getFoodQuantity()) + " (" +
                                QString::number(warriorList->at(i).getLife()) + " / " +
                                QString::number(warriorList->at(i).getMaxLife()) + ") : Home = " +
                                QString::number(warriorList->at(i).getReturnToHome()));
    }

    vector<WorkerAnt> *workerList = &this->_mainProgram->getAntFarm().getWorkerAntList();
    this->_worker->clear();
    this->_worker->addItem("Nb of Worker Ant : " + QString::number(workerList->size()));
    for (int i = 0; i < int(workerList->size()); i++) {
        this->_worker->addItem("(" +
                                QString::number(workerList->at(i).getX()) + ", " +
                                QString::number(workerList->at(i).getY()) + ") : " +
                                QString::number(workerList->at(i).getAge()) + " / " +
                                QString::number(workerList->at(i).getMaxAge()) + " (" +
                                QString::number(workerList->at(i).getLife()) + " / " +
                                QString::number(workerList->at(i).getMaxLife()) + ")");
    }

    vector<GrowingAnt> *growingList = &this->_mainProgram->getAntFarm().getGrowingAntList();
    this->_growing->clear();
    this->_growing->addItem("Nb of Growing Ant : " + QString::number(growingList->size()));
    for (int i = 0; i < int(growingList->size()); i++) {
        this->_growing->addItem("(" +
                                QString::number(growingList->at(i).getX()) + ", " +
                                QString::number(growingList->at(i).getY()) + ") : " +
                                QString::number(growingList->at(i).getAge()) + " / " +
                                QString::number(growingList->at(i).getMaxAge()) + " : " +
                                QString::fromStdString(growingList->at(i).getState()) + " (" +
                                QString::number(growingList->at(i).getLife()) + " / " +
                                QString::number(growingList->at(i).getMaxLife()) + ")");
    }
}


void MainWindow::buttonInit() {
    // Init Main Program
    this->_config->map_height = this->ui->height->text().toInt();
    this->_config->map_width = this->ui->width->text().toInt();
    this->_config->nb_source_food = this->ui->food->text().toInt();
    this->_config->nb_obstacle = this->ui->obstacle->text().toInt();
    this->_config->pheromone_evaporation_rate = this->ui->pheromone->text().toInt();
    this->_config->ant_max_life = this->ui->ant_max_life->text().toInt();
    this->_config->ant_max_age = this->ui->ant_max_age->text().toInt();
    this->_config->ant_farm_max_population = this->ui->ant_farm_max_population->text().toInt();
    this->_config->ant_farm_max_food = this->ui->ant_farm_max_food->text().toInt();
    this->_config->nb_worker_ant_init = this->ui->nb_worker_ant_init->text().toInt();
    this->_config->nb_warrior_ant_init = this->ui->nb_warrior_ant_init->text().toInt();
    this->_config->warrior_max_food = this->ui->warrior_max_food->text().toInt();

    this->_mainProgram = new MainProgram(*this->_config);

    // Init Map
    Map map = this->_mainProgram->getMap();

    this->_nb_iteration = 1;
    this->_element = new QVector<QVector<QLabel*>*>();

    QFont defaultFont = QFont( "Arial", 10, QFont::Bold);

    for (int i = 0; i < this->_config->map_height; i++) {
        this->_element->push_back(new QVector<QLabel*>());
        for (int j = 0; j < this->_config->map_width; j++) {
            Element element = map.getElement(Coord(i, j));
            QString symbol = QString::fromStdString(element.getTypeSymbol());
            QString coord = "(" + QString::number(element.getX()) + ", " + QString::number(element.getY()) + ")";
            this->_element->at(i)->append(new QLabel(symbol));
            this->_element->at(i)->at(j)->setToolTip(coord);
            this->_element->at(i)->at(j)->setFont(defaultFont);
            this->ui->map_01->addWidget(this->_element->at(i)->at(j), i, j, 1, 1);
        }
    }

    this->ui->init->setEnabled(false);
    this->ui->data_tab->removeTab(0);

    this->refreshInfo();

    this->ui->iterate->setEnabled(true);
}


void MainWindow::buttonIterate() {
    for (int i = 0; i < this->ui->nb_iteration->value(); i++)
        this->_mainProgram->iterate();

    this->_nb_iteration += this->ui->nb_iteration->value();
    this->ui->nb_iteration_label->setText(QString("Iteration : ") + QString::number(this->_nb_iteration));

    this->refreshInfo();
    this->refreshMap();
}
