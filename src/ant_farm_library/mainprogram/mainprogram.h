#ifndef MAINPROGRAM_H
#define MAINPROGRAM_H

#include <vector>
#include <iostream>
#include <random>
#include <time.h>

#include "coord/coord.h"
#include "element/element.h"
#include "food/food.h"
#include "obstacle/obstacle.h"
#include "pheromone/pheromone.h"
#include "pheromonepack/pheromonepack.h"
#include "ant/ant.h"
#include "growingant/growingant.h"
#include "queenant/queenant.h"
#include "workerant/workerant.h"
#include "warriorant/warriorant.h"
#include "antfarm/antfarm.h"
#include "map/map.h"
#include "mappheromone/mappheromone.h"
#include "config/config.h"


using namespace std;


class MainProgram
{
private:

    Map _map;
    MapPheromone _mapPheromone;
    AntFarm _antFarm;
    vector<WarriorAnt> _warriorAntList;
    vector<Food> _foodList;
    vector<Obstacle> _obstacleList;
    Config _config;

public:

    MainProgram(const Config &config);

    void iterate();

    Coord searchNextMove(const Coord &coord, const string &searchType, const bool returnToHome);

    void eatFood();
    void incrementAge();

    void displayMap();
    void displayPheromoneMap();
    void displayAntFarm();
    void displayFood();
    void displayWarriorAnt();

    Map &getMap();
    MapPheromone &getMapPheromone();
    AntFarm &getAntFarm();
    vector<Food> &getFoodList();
    vector<Obstacle> &getObstacleList();
    vector<WarriorAnt> &getWarriorAntList();
};

#endif // MAINPROGRAM_H
