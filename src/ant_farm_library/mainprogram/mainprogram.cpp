#include "mainprogram.h"


MainProgram::MainProgram(const Config &config) {
    // Setup Config
    this->_config = config;

    // Setup Map
    this->_map = Map(Coord(this->_config.map_height, this->_config.map_width));
    this->_mapPheromone = MapPheromone(Coord(this->_config.map_height, this->_config.map_width));

    // Setup Ant Farm
    Coord antFarmCoord = Coord(this->_config.map_height / 2, this->_config.map_width / 2);

    vector<WorkerAnt> workerAntList = vector<WorkerAnt>();
    for (int i = 0; i < this->_config.nb_worker_ant_init; i++)
        workerAntList.push_back(WorkerAnt(Ant(antFarmCoord, this->_config.ant_max_life, this->_config.ant_max_age)));

    this->_antFarm = AntFarm(
                QueenAnt(Ant(antFarmCoord, this->_config.ant_max_life, this->_config.ant_max_age)),
                vector<GrowingAnt>(),
                workerAntList,
                this->_config.ant_farm_max_population,
                this->_config.ant_farm_max_food,
                antFarmCoord);

    // Init Used Coord for generate objects
    vector<Coord> usedCoord = vector<Coord>();
    usedCoord.push_back(this->_antFarm.getCoord());

    // Generate list of objects
    this->_obstacleList = Obstacle::getRandomObstacle(
                config.nb_obstacle,
                this->_map.getSize(),
                usedCoord);
    for (int i = 0; i < int(this->_obstacleList.size()); i++)
        usedCoord.push_back(this->_obstacleList[i].getCoord());

    this->_foodList = Food::getRandomFood(
                this->_config.nb_source_food,
                this->_config.food_source_max_quantity,
                this->_map.getSize(),
                usedCoord);
    for (int i = 0; i < int(this->_foodList.size()); i++)
        usedCoord.push_back(this->_foodList[i].getCoord());

    this->_warriorAntList = vector<WarriorAnt>();
    for (int i = 0; i < this->_config.nb_warrior_ant_init; i++)
        this->_warriorAntList.push_back(WarriorAnt(
                                            Ant(antFarmCoord, this->_config.ant_max_life, this->_config.ant_max_age),
                                            this->_config.warrior_max_food));

    // Add all elements to Map
    this->_map.addElement(
                this->_antFarm.getCoord(),
                this->_antFarm.getElement());

    for (int i = 0; i < int(this->_foodList.size()); i++)
        this->_map.addElement(
                this->_foodList[i].getCoord(),
                this->_foodList[i].getElement());

    for (int i = 0; i < int(this->_obstacleList.size()); i++)
        this->_map.addElement(
                this->_obstacleList[i].getCoord(),
                this->_obstacleList[i].getElement());
}


void MainProgram::iterate() {
    // Start New Iteration
    for (int i = 0; i < int(this->_warriorAntList.size()); i++) {
        Coord nextMove = this->_warriorAntList[i].getCoord();

        // If we need to return to home
        if (this->_warriorAntList[i].getReturnToHome())
            nextMove = searchNextMove(this->_warriorAntList[i].getCoord(), "antfarm", this->_warriorAntList[i].getReturnToHome());

        // If we find the Ant Farm
        if (nextMove != this->_warriorAntList[i].getCoord() && this->_warriorAntList[i].getReturnToHome()) {
            nextMove = this->_warriorAntList[i].getCoord();
            this->_antFarm.setFood(this->_antFarm.getFoodQuantity() + this->_warriorAntList[i].decrementFoodQuantity(this->_config.warrior_max_food));
            this->_warriorAntList[i].setReturnToHome(false);
        } else {
            // If we not found the Ant Farm but we need to return to Home
            if (this->_warriorAntList[i].getReturnToHome())
                nextMove = searchNextMove(this->_warriorAntList[i].getCoord(), "", this->_warriorAntList[i].getReturnToHome());
            else {
                // We need to find food
                nextMove = searchNextMove(this->_warriorAntList[i].getCoord(), "food", this->_warriorAntList[i].getReturnToHome());
                // We find food
                if (nextMove != this->_warriorAntList[i].getCoord()) {
                    int food_id;
                    for (int i = 0; i < int(this->_foodList.size()); i++) {
                        if (this->_foodList[i].getCoord() == nextMove)
                            food_id = i;
                    }
                    this->_warriorAntList[i].incrementFoodQuantity(this->_foodList[food_id].decrementFoodQuantity(this->_config.warrior_max_food));
                    if (this->_warriorAntList[i].getFoodQuantity() == this->_warriorAntList[i].getMaxFoodQuantity())
                        this->_warriorAntList[i].setReturnToHome(true);
                    nextMove = this->_warriorAntList[i].getCoord();
                } else
                    nextMove = searchNextMove(this->_warriorAntList[i].getCoord(), "", this->_warriorAntList[i].getReturnToHome());
            }
        }

        // Move the Warrior Ant if possible
        if (nextMove != this->_warriorAntList[i].getCoord()) {
            Element tmpElement = this->_warriorAntList[i].getElement();
            this->_warriorAntList[i].setXY(nextMove.getX(), nextMove.getY());
            if (tmpElement.getCoord() != this->_antFarm.getCoord())
                this->_map.moveElement(tmpElement.getCoord(), nextMove);
            else
                this->_map.addElement(nextMove, tmpElement);
            // Pheromone Management
            if (this->_warriorAntList[i].getReturnToHome()) {
                this->_mapPheromone.getPheromonePack(nextMove).getHome().incrementQuantity(100);
                this->_mapPheromone.getPheromonePack(nextMove).getResearch().incrementQuantity(100);
            } else {
                this->_mapPheromone.getPheromonePack(nextMove).getHome().incrementQuantity(100);
            }
        }
    }

    // All eat food
    this->eatFood();

    // All increment Age and evolve
    this->incrementAge();

    // For Each Pheromone Pack Decrease Quantity
    this->_mapPheromone.decreaseQuantity(100 - this->_config.pheromone_evaporation_rate);

    // For Each Food
    for (int i = 0; i < int(this->_foodList.size()); i++) {
        // If food quantity = 0, then delete food source
        if (this->_foodList[i].getQuantity() == 0) {
            this->_map.removeElement(this->_foodList[i].getCoord());
            this->_foodList.erase(this->_foodList.begin() + i);
        }
    }

    // TODO IF time New Obstacle with probability of apparition
    // TODO IF time New Food source with probability of apparition
}


Coord MainProgram::searchNextMove(const Coord &coord, const string &searchType, const bool returnToHome) {
    Coord result = coord;
    vector<Coord> searchCoord = vector<Coord>();

    if (coord.getY() + 1 < this->_map.getSize().getY() - 1 && coord.getY() + 1 > 0)
        searchCoord.push_back(Coord(coord.getX(), coord.getY() + 1));
    if (coord.getY() - 1 < this->_map.getSize().getY() - 1 && coord.getY() - 1 > 0)
        searchCoord.push_back(Coord(coord.getX(), coord.getY() - 1));
    if (coord.getX() + 1 < this->_map.getSize().getX() - 1 && coord.getX() + 1 > 0)
        searchCoord.push_back(Coord(coord.getX() + 1, coord.getY()));
    if (coord.getX() - 1 < this->_map.getSize().getX() - 1 && coord.getX() - 1 > 0)
        searchCoord.push_back(Coord(coord.getX() - 1, coord.getY()));

    vector<Coord> saveSearchCoord = vector<Coord>();

    for (int i = 0; i < int(searchCoord.size()); i++) {
        if (this->_map.getElement(searchCoord[i]).getType() == searchType)
            saveSearchCoord.push_back(this->_map.getElement(searchCoord[i]).getCoord());
    }

    srand((unsigned) time(0));

    if (int(saveSearchCoord.size()) > 0) {
        if (int(saveSearchCoord.size()) == 1)
            result = saveSearchCoord[0];
        else {
            // Random System
            result = saveSearchCoord[int(rand() % static_cast<int>(saveSearchCoord.size() - 1))];
            PheromonePack choicePheromone = this->_mapPheromone.getPheromonePack(result);
            // Pheromone Check
            for (int i = 0; i < int(saveSearchCoord.size()); i++) {
                PheromonePack tmpPheromone = this->_mapPheromone.getPheromonePack(saveSearchCoord[i].getCoord());
                if (returnToHome) {
                    if (tmpPheromone.getHome().getQuantity() > choicePheromone.getHome().getQuantity())
                        result = saveSearchCoord[i];
                } else {
                    if (tmpPheromone.getResearch().getQuantity() > choicePheromone.getResearch().getQuantity())
                        result = saveSearchCoord[i];
                }
            }
            // Random Final Choice
            if (int(rand() % static_cast<int>(100) > 75))
                result = saveSearchCoord[int(rand() % static_cast<int>(saveSearchCoord.size() - 1))];
        }
    }

    return result;
}

void MainProgram::eatFood() {
    // For Each Warrior Ant (Eat Food)
    for (int i = 0; i < int(this->_warriorAntList.size()); i++) {
        // Eat Food, else lose 1 HP (if HP = 0 then die and delete)
        int foodWarrior = this->_warriorAntList[i].decrementFoodQuantity(1);
        if (foodWarrior > 0)
            this->_warriorAntList[i].incrementLife();
        else {
            this->_warriorAntList[i].setReturnToHome(false);
            this->_warriorAntList[i].decrementLife();
            // If life = 0 then die
            if (this->_warriorAntList[i].getLife() == 0) {
                this->_map.removeElement(this->_warriorAntList[i].getCoord());
                this->_warriorAntList.erase(this->_warriorAntList.begin() + i);
            }
        }
    }

    // Queen Eat Food, else lose 1 HP (if HP = 0 then die and delete)
    if (this->_antFarm.getQueenAnt().getLife() > 0) {
        int foodQueen = this->_antFarm.decrementFood(1);
        if (foodQueen > 0)
            this->_antFarm.getQueenAnt().incrementLife();
        else
            this->_antFarm.getQueenAnt().decrementLife();
    }

    // For Each Growing Ant
    vector<GrowingAnt> *growingAntList = &this->_antFarm.getGrowingAntList();
    for (int i = 0; i < int(growingAntList->size()); i++) {
        // Eat Food, else lose 1 HP (if HP = 0 then die and delete)
        int foodGrowing = this->_antFarm.decrementFood(1);
        if (foodGrowing > 0)
            growingAntList->at(i).incrementLife();
        else {
            growingAntList->at(i).decrementLife();
            if (growingAntList->at(i).getLife() == 0)
                growingAntList->erase(growingAntList->begin() + i);
        }
    }

    // For Each Worker Ant
    vector<WorkerAnt> *workerAntList = &this->_antFarm.getWorkerAntList();
    for (int i = 0; i < int(workerAntList->size()); i++) {
        // Eat Food, else lose 1 HP (if HP = 0 then die and delete)
        int foodWorker = this->_antFarm.decrementFood(1);
        if (foodWorker > 0)
            workerAntList->at(i).incrementLife();
        else {
            workerAntList->at(i).decrementLife();
            if (workerAntList->at(i).getLife() == 0)
                workerAntList->erase(workerAntList->begin() + i);
        }
    }

    // If food is available and Life > 0 Queen create new eggs
    if (this->_antFarm.getQueenAnt().getLife() > 0) {
        if (this->_antFarm.getPopulation() < this->_antFarm.getMaxPopulation()) {
            for (int i = 0; i < this->_config.nb_eggs_max_per_iteration; i++) {
                if(this->_antFarm.getFoodQuantity() > this->_config.nb_food_ant_creation) {
                    // Queen Eat Food and create eggs
                    this->_antFarm.decrementFood(this->_config.nb_food_ant_creation);
                    growingAntList->push_back(GrowingAnt(Ant(this->_antFarm.getCoord(), this->_config.ant_max_life, this->_config.ant_max_age)));
                }
            }
        }
    }
}

void MainProgram::incrementAge() {
    // For Each Warrior Ant (Increment Age)
    for (int i = 0; i < int(this->_warriorAntList.size()); i++)
        this->_warriorAntList[i].incrementAge();

    // Queen Ant (Increment Age)
    if (this->_antFarm.getQueenAnt().getLife() > 0)
        this->_antFarm.getQueenAnt().incrementAge();
    /*
    if (this->_antFarm.getQueenAnt().getAge() == this->_antFarm.getQueenAnt().getMaxAge())
        this->_antFarm.getQueenAnt().decrementLife();*/

    // Worker Ant (Increment Age and evolve)
    vector<WorkerAnt> *workerAntList = &this->_antFarm.getWorkerAntList();
    for (int i = 0; i < int(workerAntList->size()); i++) {
        workerAntList->at(i).incrementAge();
        // Grow to Warrior
        if (workerAntList->at(i).getAge() >= this->_config.worker_stage_duration) {
            this->_warriorAntList.push_back(WarriorAnt(workerAntList->at(i).getAnt(), this->_config.warrior_max_food));
            workerAntList->erase(workerAntList->begin() + i);
        }
    }

    // Growing Ant (Increment Age and evolve)
    vector<GrowingAnt> *growingAntList = &this->_antFarm.getGrowingAntList();
    for (int i = 0; i < int(growingAntList->size()); i++) {
        growingAntList->at(i).incrementAge();
        // Grow to Larva
        if (growingAntList->at(i).getAge() >= this->_config.incubation_time && growingAntList->at(i).getState() == "egg")
            growingAntList->at(i).upgradeState();
        // Grow to Worker Ant
        if (growingAntList->at(i).getAge() >= this->_config.incubation_time + this->_config.larval_stage_duration) {
            workerAntList->push_back(WorkerAnt(growingAntList->at(i).getAnt()));
            growingAntList->erase(growingAntList->begin() + i);
        }
    }
}


void MainProgram::displayMap() {
    Coord mapSize = this->_map.getSize();
    for (int i = 0; i < mapSize.getX(); i++) {
        cout << "|";
        for (int j = 0; j < mapSize.getY(); j++) {
            string symbol = this->_map.getElement(Coord(i, j)).getTypeSymbol();
            if (symbol == "")
                cout << " ";
            else
                cout << symbol;
        }
        cout << "|" << endl;
    }
}

void MainProgram::displayPheromoneMap() {
    Coord mapSize = this->_mapPheromone.getSize();
    for (int i = 0; i < mapSize.getX(); i++) {
        cout << "|";
        for (int j = 0; j < mapSize.getY(); j++) {
            PheromonePack tmp = this->_mapPheromone.getPheromonePack(Coord(i, j));
            cout << tmp.getResearch().getQuantity() << "-" << tmp.getHome().getQuantity() << "|";
        }
        cout << endl;
    }
}

void MainProgram::displayAntFarm() {
    cout << "Ant Farm Food Quantity = " << this->_antFarm.getFoodQuantity() << endl;
}

void MainProgram::displayFood() {
    for (int i = 0; i < int(this->_foodList.size()); i++) {
        Food tmp = this->_foodList[i];
        cout << "Food (" << tmp.getX() << ", " << tmp.getY() << ") : " << tmp.getQuantity() << endl;
    }
}

void MainProgram::displayWarriorAnt() {
    for (int i = 0; i < int(this->_warriorAntList.size()); i++) {
        WarriorAnt tmp = this->_warriorAntList[i];
        cout << "Warrior Ant (" << tmp.getX() << ", " << tmp.getY() << ") : " << tmp.getFoodQuantity() << endl;
    }
}


Map &MainProgram::getMap() { return this->_map; }

MapPheromone &MainProgram::getMapPheromone() { return this->_mapPheromone; }

AntFarm &MainProgram::getAntFarm() { return this->_antFarm; }

vector<Food> &MainProgram::getFoodList() { return this->_foodList; }

vector<Obstacle> &MainProgram::getObstacleList() { return this->_obstacleList; }

vector<WarriorAnt> &MainProgram::getWarriorAntList() { return this->_warriorAntList; }
