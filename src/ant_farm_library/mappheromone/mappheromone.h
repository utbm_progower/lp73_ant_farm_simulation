#ifndef MAPPHEROMONE_H
#define MAPPHEROMONE_H

#include <vector>

#include "element/element.h"
#include "coord/coord.h"
#include "pheromone/pheromone.h"
#include "pheromonepack/pheromonepack.h"


class MapPheromone
{
private:

    vector<vector<PheromonePack>> _map;
    Coord _coord;

public:

    MapPheromone();
    MapPheromone(const Coord &coord);

    PheromonePack& getPheromonePack(const Coord &coord);

    Coord getSize() const;
    void decreaseQuantity(const int quantity);
};

#endif // MAPPHEROMONE_H
