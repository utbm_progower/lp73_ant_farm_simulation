#include "mappheromone.h"


MapPheromone::MapPheromone() {
    this->_map = vector<vector<PheromonePack>>();
    this->_coord = Coord();
}

MapPheromone::MapPheromone(const Coord &coord) {
    this->_coord = Coord(coord);
    this->_map = vector<vector<PheromonePack>>(this->_coord.getX());

    for (int i = 0; i < this->_coord.getX(); i++) {
        this->_map[i] = vector<PheromonePack>(this->_coord.getY());
    }
}


PheromonePack& MapPheromone::getPheromonePack(const Coord &coord) {
    return this->_map[coord.getX()][coord.getY()];
}


Coord MapPheromone::getSize() const { return this->_coord; }

void MapPheromone::decreaseQuantity(const int quantity) {
    for (int i = 0; i < this->_coord.getX(); i++) {
        for (int j = 0; j < this->_coord.getY(); j++) {
            this->_map[i][j].getHome().decrementQuantity(quantity);
            this->_map[i][j].getResearch().decrementQuantity(quantity);
        }
    }
}
