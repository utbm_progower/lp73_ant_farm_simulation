#include "growingant.h"


GrowingAnt::GrowingAnt() : Ant(Ant(), "growingAnt") {
    this->_state = 0;
}

GrowingAnt::GrowingAnt(const Ant &ant) : Ant(ant, "growingAnt") {
    this->_state = 0;
}

GrowingAnt::GrowingAnt(const GrowingAnt &growingAnt) : Ant(growingAnt.getAnt()) {
    this->_state = growingAnt._state;
}


string GrowingAnt::getState() const {
    string state = "";

    switch (this->_state) {
        case 0:
            state = "egg";
        break;
        case 1:
            state = "larva";
        break;
    }

    return state;
}

void GrowingAnt::upgradeState() {
    if (this->_state >= 1)
        this->_state = 1;
    else
        this->_state++;
}
