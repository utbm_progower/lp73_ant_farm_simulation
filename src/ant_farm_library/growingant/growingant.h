#ifndef GROWINGANT_H
#define GROWINGANT_H

#include "ant/ant.h"


class GrowingAnt : public Ant
{
private:

    int _state;

public:

    GrowingAnt();
    GrowingAnt(const Ant &ant);
    GrowingAnt(const GrowingAnt &growingAnt);

    string getState() const;
    void upgradeState();
};

#endif // GROWINGANT_H
