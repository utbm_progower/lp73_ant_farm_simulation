#include "element.h"


Element::Element() : Coord() { this->_type = ""; }

Element::Element(const Coord &coord, const string &type) : Coord(coord) {
    this->_type = type;
}

Element::Element(const Element &element) : Coord(element.getCoord()) {
    this->_type = element._type;
}


string Element::getType() const { return this->_type; }

string Element::getTypeSymbol() const {
    string symbol = "";

    if (this->_type == "obstacle")
        symbol = "#";
    if (this->_type == "food")
        symbol = "0";
    if (this->_type == "antfarm")
        symbol = "H";
    if (this->_type == "warriorant")
        symbol = "W";

    return symbol;
}

void Element::setType(const string &type) { this->_type = type; }

Element Element::getElement() const { return Element(*this); }


Element &Element::operator=(const Element &element) {
    if (this != &element) {
        this->_x = element._x;
        this->_y = element._y;
        this->_type = element._type;
    }
    return *this;
}
