#ifndef ELEMENT_H
#define ELEMENT_H

#include <iostream>
#include "coord/coord.h"


using namespace std;


class Element : public Coord
{
protected:

    string _type;

public:

    Element();
    Element(const Coord &coord, const string &type);
    Element(const Element &element);

    string getType() const;
    string getTypeSymbol() const;
    void setType(const string &type);
    Element getElement() const;

    Element &operator=(const Element &element);
};


#endif // ELEMENT_H
