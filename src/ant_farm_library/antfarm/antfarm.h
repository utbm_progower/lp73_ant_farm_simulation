#ifndef ANTFARM_H
#define ANTFARM_H

#include <vector>

#include "element/element.h"
#include "coord/coord.h"
#include "queenant/queenant.h"
#include "growingant/growingant.h"
#include "workerant/workerant.h"


class AntFarm : public Element
{
private:

    int _max_population;
    int _max_food_quantity;
    int _food_quantity;
    QueenAnt _queenAnt;
    vector<GrowingAnt> _growingAntList;
    vector<WorkerAnt> _workerAntList;

public:

    AntFarm();
    AntFarm(const QueenAnt &queenAnt,
            const vector<GrowingAnt> &growingAntList,
            const vector<WorkerAnt> &workerAntList,
            const int max_population,
            const int max_food_quantity,
            const Coord &coord);

    int getPopulation() const;
    int getMaxPopulation() const;

    int getFoodQuantity() const;
    int getMaxFoodQuantity() const;
    int decrementFood(const int quantity);
    void setFood(const int quantity);

    QueenAnt& getQueenAnt();
    vector<GrowingAnt>& getGrowingAntList();
    vector<WorkerAnt>& getWorkerAntList();
};

#endif // ANTFARM_H
