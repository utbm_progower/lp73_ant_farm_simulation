#include "antfarm.h"


AntFarm::AntFarm() {
    this->_max_population = 0;
    this->_max_food_quantity = 0;
    this->_food_quantity = 0;
    this->_queenAnt = QueenAnt();
    this->_growingAntList = vector<GrowingAnt>();
    this->_workerAntList = vector<WorkerAnt>();
}

AntFarm::AntFarm(const QueenAnt &queenAnt,
                 const vector<GrowingAnt> &growingAntList,
                 const vector<WorkerAnt> &workerAntList,
                 const int max_population,
                 const int max_food_quantity,
                 const Coord &coord) : Element(coord, "antfarm")
{
    this->_max_population = max_population;
    this->_max_food_quantity = max_food_quantity;
    this->_food_quantity = 0.25 * max_food_quantity;
    this->_queenAnt = queenAnt;
    this->_growingAntList = growingAntList;
    this->_workerAntList = workerAntList;
}


int AntFarm::getPopulation() const {
    int population = 0;
    if (this->_queenAnt.getLife() > 0)
        population += 1;
    population += this->_growingAntList.size();
    population += this->_workerAntList.size();
    return population;
}

int AntFarm::getMaxPopulation() const { return this->_max_population; }


int AntFarm::getFoodQuantity() const { return this->_food_quantity; }

int AntFarm::getMaxFoodQuantity() const { return this->_max_food_quantity; }

int AntFarm::decrementFood(const int quantity) {
    int food = 0;
    if (quantity > 0) {
        if (quantity >= this->_food_quantity) {
            food = this->_food_quantity;
            this->_food_quantity = 0;
        } else {
            food = quantity;
            this->_food_quantity = this->_food_quantity - quantity;
        }
    }
    return food;
}

void AntFarm::setFood(const int quantity) {
    if (quantity >= 0)
        this->_food_quantity = quantity;
    if (this->_food_quantity >= this->_max_food_quantity)
        this->_food_quantity = this->_max_food_quantity;
}


QueenAnt& AntFarm::getQueenAnt() { return this->_queenAnt; }

vector<GrowingAnt>& AntFarm::getGrowingAntList() { return this->_growingAntList; }

vector<WorkerAnt>& AntFarm::getWorkerAntList() { return this->_workerAntList; }
