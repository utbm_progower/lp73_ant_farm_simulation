#ifndef WORKERANT_H
#define WORKERANT_H

#include "ant/ant.h"


class WorkerAnt : public Ant
{
public:

    WorkerAnt();
    WorkerAnt(const Ant &ant);
};

#endif // WORKERANT_H
