#include "coord.h"


Coord::Coord() {
    this->_x = 0;
    this->_y = 0;
}

Coord::Coord(const int x, const int y) {
    this->_x = x;
    this->_y = y;
}

Coord::Coord(const Coord &coord) {
    this->_x = coord._x;
    this->_y = coord._y;
}


int Coord::getX() const { return this->_x; }

void Coord::setX(const int x) { this->_x = x; }


int Coord::getY() const { return this->_y; }

void Coord::setY(const int y) { this->_y = y; }


void Coord::setXY(const int x, const int y) {
    this->_x = x;
    this->_y = y;
}

Coord Coord::getCoord() const { return Coord(*this); }


bool Coord::isEqual(const Coord &coord) const {
    bool res = true;
    if (this->_x != coord._x)
        res = false;
    if (this->_y != coord._y)
        res = false;
    return res;
}


bool Coord::operator==(const Coord &coord) const {
    bool res;
    if (isEqual(coord))
        res = true;
    else
        res = false;
    return res;
}

bool Coord::operator!=(const Coord &coord) const {
    bool res;
    if (isEqual(coord))
        res = false;
    else
        res = true;
    return res;
}


Coord &Coord::operator=(const Coord &coord) {
    this->_x = coord._x;
    this->_y = coord._y;
    return *this;
}
