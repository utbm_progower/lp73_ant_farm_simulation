#ifndef COORD_H
#define COORD_H


class Coord
{
protected:

    int _x;
    int _y;

public:

    Coord();
    Coord(const int x, const int y);
    Coord(const Coord &coord);

    int getX() const;
    void setX(const int x);

    int getY() const;
    void setY(const int y);

    void setXY(const int x, const int y);
    Coord getCoord() const;

    bool isEqual(const Coord &coord) const;

    bool operator==(const Coord &coord) const;
    bool operator!=(const Coord &coord) const;

    Coord &operator=(const Coord &coord);
};


#endif // COORD_H
