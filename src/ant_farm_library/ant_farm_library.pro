CONFIG -= qt

TEMPLATE = lib
DEFINES += ANT_FARM_LIBRARY_LIBRARY

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ant/ant.cpp \
    antfarm/antfarm.cpp \
    config/config.cpp \
    coord/coord.cpp \
    element/element.cpp \
    food/food.cpp \
    growingant/growingant.cpp \
    mainprogram/mainprogram.cpp \
    map/map.cpp \
    mappheromone/mappheromone.cpp \
    obstacle/obstacle.cpp \
    pheromone/pheromone.cpp \
    pheromonepack/pheromonepack.cpp \
    queenant/queenant.cpp \
    warriorant/warriorant.cpp \
    workerant/workerant.cpp

HEADERS += \
    ant/ant.h \
    ant_farm_library_global.h \
    antfarm/antfarm.h \
    config/config.h \
    coord/coord.h \
    element/element.h \
    food/food.h \
    growingant/growingant.h \
    mainprogram/mainprogram.h \
    map/map.h \
    mappheromone/mappheromone.h \
    obstacle/obstacle.h \
    pheromone/pheromone.h \
    pheromonepack/pheromonepack.h \
    queenant/queenant.h \
    warriorant/warriorant.h \
    workerant/workerant.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
