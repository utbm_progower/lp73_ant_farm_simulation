#ifndef PHEROMONEPACK_H
#define PHEROMONEPACK_H

#include "element/element.h"
#include "coord/coord.h"
#include "pheromone/pheromone.h"


class PheromonePack : public Element
{
private:

    Pheromone _research_pheromone;
    Pheromone _home_pheromone;

public:

    PheromonePack();
    PheromonePack(const Coord &coord,
                  const int researchQuantity,
                  const int homeQuantity);
    PheromonePack(const PheromonePack &pheromonePack);

    Pheromone& getResearch();
    Pheromone& getHome();

    PheromonePack &operator=(const PheromonePack &pheromonePack);
};

#endif // PHEROMONEPACK_H
