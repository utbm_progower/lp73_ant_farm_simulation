#include "pheromonepack.h"


PheromonePack::PheromonePack() : Element(Coord(), "pheromonePack") {
    this->_research_pheromone = Pheromone();
    this->_home_pheromone = Pheromone();
}

PheromonePack::PheromonePack(const Coord &coord,
                             const int researchQuantity,
                             const int homeQuantity) : Element(coord, "pheromonePack") {
    this->_research_pheromone = Pheromone(coord, researchQuantity);
    this->_home_pheromone = Pheromone(coord, homeQuantity);
}

PheromonePack::PheromonePack(const PheromonePack &pheromonePack) : Element(pheromonePack.getCoord(), "pheromonePack") {
    this->_research_pheromone = pheromonePack._research_pheromone;
    this->_home_pheromone = pheromonePack._home_pheromone;
}


Pheromone& PheromonePack::getResearch() { return this->_research_pheromone; }

Pheromone& PheromonePack::getHome() { return this->_home_pheromone; }


PheromonePack &PheromonePack::operator=(const PheromonePack &pheromonePack) {
    this->_research_pheromone = pheromonePack._research_pheromone;
    this->_home_pheromone = pheromonePack._home_pheromone;
    return *this;
}
