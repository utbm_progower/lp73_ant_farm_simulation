#include "warriorant.h"


WarriorAnt::WarriorAnt() : Ant(Ant(), "warriorant") {
    this->_food_quantity = 0;
    this->_max_food_quantity = 0;
    this->_return_to_home = false;
}

WarriorAnt::WarriorAnt(const Ant &ant) : Ant(ant, "warriorant") {
    this->_food_quantity = 0;
    this->_max_food_quantity = 0;
    this->_return_to_home = false;
}

WarriorAnt::WarriorAnt(const Ant &ant, const int max_food_quantity) : Ant(ant, "warriorant") {
    this->_food_quantity = 0;
    this->_max_food_quantity = max_food_quantity;
    this->_return_to_home = false;
}


int WarriorAnt::getFoodQuantity() const { return this->_food_quantity; }

void WarriorAnt::setFoodQuantity(const int quantity) {
    if (quantity > 0)
        this->_food_quantity = quantity;
}


int WarriorAnt::getMaxFoodQuantity() const { return this->_max_food_quantity; }

void WarriorAnt::setMaxFoodQuantity(const int max_quantity) {
    if (max_quantity > 0)
        this->_max_food_quantity = max_quantity;
}


int WarriorAnt::incrementFoodQuantity(const int quantity) {
    int food = 0;
    if (quantity > 0) {
        if (quantity > this->_max_food_quantity - this->_food_quantity) {
            food = this->_max_food_quantity - this->_food_quantity;
            this->_food_quantity = this->_max_food_quantity;
        } else {
            food = quantity;
            this->_food_quantity = this->_food_quantity + quantity;
        }
    }
    return food;
}

int WarriorAnt::decrementFoodQuantity(const int quantity) {
    int food = 0;
    if (quantity > 0) {
        if (quantity > this->_food_quantity) {
            food = this->_food_quantity;
            this->_food_quantity = 0;
        } else {
            food = quantity;
            this->_food_quantity = this->_food_quantity - quantity;
        }
    }
    return food;
}


bool WarriorAnt::getReturnToHome() const { return this->_return_to_home; }

void WarriorAnt::setReturnToHome(const bool home) { this->_return_to_home = home; }
