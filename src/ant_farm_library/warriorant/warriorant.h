#ifndef WARRIORANT_H
#define WARRIORANT_H

#include "ant/ant.h"


class WarriorAnt : public Ant
{
private:

    int _food_quantity;
    int _max_food_quantity;
    bool _return_to_home;

public:

    WarriorAnt();
    WarriorAnt(const Ant &ant);
    WarriorAnt(const Ant &ant, const int max_food_quantity);

    int getFoodQuantity() const;
    void setFoodQuantity(const int quantity);

    int getMaxFoodQuantity() const;
    void setMaxFoodQuantity(const int max_quantity);

    int incrementFoodQuantity(const int quantity);
    int decrementFoodQuantity(const int quantity);

    bool getReturnToHome() const;
    void setReturnToHome(const bool home);
};

#endif // WARRIORANT_H
