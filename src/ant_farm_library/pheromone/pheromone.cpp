#include "pheromone.h"


Pheromone::Pheromone() : Element(Coord(), "pheromone") {
    this->_quantity = 0;
}

Pheromone::Pheromone(const Coord &coord, const int quantity) : Element(coord, "pheromone") {
    this->_quantity = quantity;
}

Pheromone::Pheromone(const Pheromone &pheromone) : Element(pheromone.getCoord(), "pheromone") {
    this->_quantity = pheromone._quantity;
}


int Pheromone::getQuantity() const { return this->_quantity; }

void Pheromone::setQuantity(const int quantity) { this->_quantity = quantity; }


void Pheromone::incrementQuantity(const int quantity) {
    if (quantity > 0)
        this->_quantity = this->_quantity + quantity;
}

int Pheromone::decrementQuantity(const int quantity) {
    int pheromone = 0;
    if (quantity > 0) {
        if (quantity > this->_quantity) {
            pheromone = this->_quantity;
            this->_quantity = 0;
        } else {
            pheromone = quantity;
            this->_quantity = this->_quantity - quantity;
        }
    }
    return pheromone;
}


Pheromone& Pheromone::operator=(const Pheromone &pheromone) {
    this->_quantity = pheromone._quantity;
    return *this;
}
