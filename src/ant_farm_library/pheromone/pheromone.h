#ifndef PHEROMONE_H
#define PHEROMONE_H

#include "element/element.h"
#include "coord/coord.h"


class Pheromone : public Element
{
private:

    int _quantity;

public:

    Pheromone();
    Pheromone(const Coord &coord, const int quantity);
    Pheromone(const Pheromone &pheromone);

    int getQuantity() const;
    void setQuantity(const int quantity);

    void incrementQuantity(const int quantity);
    int decrementQuantity(const int quantity);

    Pheromone &operator=(const Pheromone &pheromone);
};


#endif // PHEROMONE_H
