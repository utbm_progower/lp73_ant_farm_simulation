#ifndef QUEENANT_H
#define QUEENANT_H

#include "ant/ant.h"


class QueenAnt : public Ant
{
public:

    QueenAnt();
    QueenAnt(const Ant &ant);
};

#endif // QUEENANT_H
