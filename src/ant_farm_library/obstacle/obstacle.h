#ifndef OBSTACLE_H
#define OBSTACLE_H

#include <vector>
#include <random>
#include <time.h>

#include "element/element.h"
#include "coord/coord.h"


class Obstacle : public Element
{
public:

    Obstacle();
    Obstacle(const Coord &coord);

    static vector<Obstacle> getRandomObstacle(
            const int nb,
            const Coord &map_size,
            vector<Coord> &avoid_coord);

};

#endif // OBSTACLE_H
