#include "obstacle.h"


Obstacle::Obstacle() : Element(Coord(), "obstacle") {}

Obstacle::Obstacle(const Coord &coord) : Element(coord, "obstacle") {}


vector<Obstacle> Obstacle::getRandomObstacle(
        const int nb,
        const Coord &map_size,
        vector<Coord> &avoid_coord) {
    vector<Obstacle> obstacleList = vector<Obstacle>();

    srand((unsigned) time(0));

    for (int i = 0; i < nb; i++) {
        bool findCoord = false;
        Coord tmpCoord = Coord();
        // Find Coord
        while (!findCoord) {
            tmpCoord.setXY(
                        rand() % static_cast<int>(map_size.getX()),
                        rand() % static_cast<int>(map_size.getY()));

            // Check Avoid Coord
            bool check_avoid = false;
            int j = 0;
            while (j < int(avoid_coord.size()) && !check_avoid) {
                if (tmpCoord == avoid_coord[j])
                    check_avoid = true;
                j++;
            }

            // Check Duplicate
            bool check_duplicate = false;
            for (int n = 0; n < int(obstacleList.size()); n++) {
                if (obstacleList[n].getCoord() == tmpCoord)
                    check_duplicate = true;
            }

            if (!check_avoid && !check_duplicate)
                findCoord = true;
        }
        obstacleList.push_back(Obstacle(tmpCoord));
    }

    return obstacleList;
}
