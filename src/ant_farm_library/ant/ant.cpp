#include "ant.h"


Ant::Ant() : Element(Coord(), "ant") {
    this->_life = 0;
    this->_max_life = 0;
    this->_age = 0;
    this->_max_age = 0;
}

Ant::Ant(const Coord &coord, const int max_life, const int max_age) : Element(coord, "ant") {
    this->_life = max_life;
    this->_max_life = max_life;
    this->_age = 0;
    this->_max_age = max_age;
}

Ant::Ant(const Ant &ant) : Element(Coord(ant.getCoord()), ant.getType()) {
    this->_life = ant._max_life;
    this->_max_life = ant._max_life;
    this->_age = ant._age;
    this->_max_age = ant._max_age;
}

Ant::Ant(const Ant &ant, string type) : Element(Coord(ant.getCoord()), type) {
    this->_life = ant._max_life;
    this->_max_life = ant._max_life;
    this->_age = ant._age;
    this->_max_age = ant._max_age;
}


int Ant::getLife() const { return this->_life; }

void Ant::incrementLife() {
    this->_life = this->_life + 1;
    if (this->_life >= this->_max_life)
        this->_life = this->_max_life;
}

void Ant::decrementLife() {
    this->_life = this->_life - 1;
    if (this->_life <= 0)
        this->_life = 0;
}

int Ant::getMaxLife() const { return this->_max_life; }


int Ant::getMaxAge() const { return this->_max_age; }

int Ant::getAge() const { return this->_age; }

void Ant::incrementAge() {
    this->_age = this->_age + 1;
    if (this->_age >= this->_max_age)
        this->_age = this->_max_age;
}


Ant Ant::getAnt() const { return Ant(*this); }
