#ifndef ANT_H
#define ANT_H

#include "element/element.h"
#include "coord/coord.h"


class Ant : public Element
{
protected:

    int _life;
    int _max_life;
    int _age;
    int _max_age;

public:

    Ant();
    Ant(const Coord &coord, const int max_life, const int max_age);
    Ant(const Ant &ant);
    Ant(const Ant &ant, string type);

    int getLife() const;
    int getMaxLife() const;
    void incrementLife();
    void decrementLife();

    int getAge() const;
    int getMaxAge() const;
    void incrementAge();

    Ant getAnt() const;
};

#endif // ANT_H
