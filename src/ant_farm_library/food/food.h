#ifndef FOOD_H
#define FOOD_H

#include <vector>
#include <time.h>

#include "element/element.h"
#include "coord/coord.h"


class Food : public Element
{
private:

    int _quantity;

public:

    Food();
    Food(const Coord &coord, const int quantity);
    Food(const Food &food);

    int getQuantity() const;
    void decrementFood();
    int decrementFoodQuantity(const int quantity);

    static vector<Food> getRandomFood(
            const int nb,
            const int max_quantity,
            const Coord &map_size,
            vector<Coord> &avoid_coord);
};

#endif // FOOD_H
