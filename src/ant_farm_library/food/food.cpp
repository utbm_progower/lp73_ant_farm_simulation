#include "food.h"


Food::Food() : Element(Coord(), "food") { this->_quantity = 0; }

Food::Food(const Coord &coord, const int quantity) : Element(coord, "food") {
    this->_quantity = quantity;
}

Food::Food(const Food &food) : Element(food.getElement()) {
    this->_quantity = food._quantity;
}


int Food::getQuantity() const { return this->_quantity; }

void Food::decrementFood() {
    if (this->_quantity <= 0)
        this->_quantity = 0;
    else
        this->_quantity--;
}

int Food::decrementFoodQuantity(const int quantity) {
    int food = 0;
    if (quantity > 0) {
        if (quantity > this->_quantity) {
            food = this->_quantity;
            this->_quantity = 0;
        } else {
            food = quantity;
            this->_quantity = this->_quantity - quantity;
        }
    }
    return food;
}


vector<Food> Food::getRandomFood(
        const int nb,
        const int max_quantity,
        const Coord &map_size,
        vector<Coord> &avoid_coord) {
    vector<Food> foodList = vector<Food>();

    srand((unsigned) time(0));

    for (int i = 0; i < nb; i++) {
        bool findCoord = false;
        Coord tmpCoord = Coord();
        // Find Coord
        while (!findCoord) {
            tmpCoord.setXY(
                        rand() % static_cast<int>(map_size.getX()),
                        rand() % static_cast<int>(map_size.getY()));

            // Check Avoid Coord
            bool check_avoid = false;
            int j = 0;
            while (j < int(avoid_coord.size()) && !check_avoid) {
                if (tmpCoord == avoid_coord[j])
                    check_avoid = true;
                j++;
            }

            // Check Duplicate
            bool check_duplicate = false;
            for (int n = 0; n < int(foodList.size()); n++) {
                if (foodList[n].getCoord() == tmpCoord)
                    check_duplicate = true;
            }

            if (!check_avoid && !check_duplicate)
                findCoord = true;
        }
        foodList.push_back(Food(tmpCoord, rand() % static_cast<int>(max_quantity)));
    }

    return foodList;
}
