#ifndef MAP_H
#define MAP_H

#include <vector>

#include "element/element.h"
#include "coord/coord.h"


class Map
{
private:

    vector<vector<Element>> _map;
    Coord _coord;

public:

    Map();
    Map(const Coord &coord);

    Element& getElement(const Coord &coord);

    Coord getSize() const;

    void addElement(const Coord &coord, const Element &element);
    Element removeElement(const Coord &coord);
    void moveElement(const Coord &oldCoord, const Coord &newCoord);

    bool isEmpty(const Coord &coord) const;
};

#endif // MAP_H
