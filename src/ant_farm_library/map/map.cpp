#include "map.h"


Map::Map() {
    this->_coord = Coord(10, 10);
    this->_map = vector<vector<Element>>(this->_coord.getX());
    for (int i = 0; i < this->_coord.getX(); i++)
        this->_map[i] = vector<Element>(this->_coord.getY());

    for (int i = 0; i < this->_coord.getX(); i++) {
        for (int j = 0; j < this->_coord.getY(); j++)
            this->_map[i][j] = Element(Coord(i, j), "");
    }
}

Map::Map(const Coord &coord) {
    this->_coord = Coord(coord);
    this->_map = vector<vector<Element>>(this->_coord.getX());
    for (int i = 0; i < this->_coord.getX(); i++)
        this->_map[i] = vector<Element>(this->_coord.getY());

    for (int i = 0; i < this->_coord.getX(); i++) {
        for (int j = 0; j < this->_coord.getY(); j++)
            this->_map[i][j] = Element(Coord(i, j), "");
    }
}


Element& Map::getElement(const Coord &coord) {
    return this->_map[coord.getX()][coord.getY()];
}


Coord Map::getSize() const { return this->_coord; }


void Map::addElement(const Coord &coord, const Element &element) {
    this->_map[coord.getX()][coord.getY()] = element;
}

Element Map::removeElement(const Coord &coord) {
    Element element = this->getElement(coord);
    this->_map[coord.getX()][coord.getY()] = Element(coord, "");
    return element;
}

void Map::moveElement(const Coord &oldCoord, const Coord &newCoord) {
    Element element = this->getElement(oldCoord);
    this->removeElement(oldCoord);
    element.setXY(newCoord.getX(), newCoord.getY());
    this->addElement(newCoord, element);
}


bool Map::isEmpty(const Coord &coord) const {
    if (this->_map[coord.getX()][coord.getY()].getType() == "")
        return true;
    else
        return false;
}
