#include "config.h"


Config::Config() {
    this->map_height = 50; // 10
    this->map_width = 100; // 30
    this->nb_obstacle = 1000; // 10
    this->nb_source_food = 500; // 50
    this->pheromone_evaporation_rate = 95;
    this->ant_max_life = 100;
    this->ant_max_age = 1000;
    this->ant_farm_max_population = 100;
    this->ant_farm_max_food = 10000;
    this->ant_queen_food_consumption = 3;
    this->ant_food_consumption = 1;
    this->nb_food_ant_creation = 10;
    this->nb_eggs_max_per_iteration = 3;
    this->incubation_time = 10;
    this->larval_stage_duration = 10;
    this->worker_stage_duration = 300;
    this->nb_worker_ant_init = 10;
    this->nb_warrior_ant_init = 30; // 3
    this->food_source_max_quantity = 1000;
    this->warrior_max_food = 50;
}

Config::Config(const Config &config) {
    this->map_height = config.map_height;
    this->map_width = config.map_width;
    this->nb_obstacle = config.nb_obstacle;
    this->nb_source_food = config.nb_source_food;
    this->pheromone_evaporation_rate = config.pheromone_evaporation_rate;
    this->ant_max_life = config.ant_max_life;
    this->ant_max_age = config.ant_max_age;
    this->ant_farm_max_population = config.ant_farm_max_population;
    this->ant_farm_max_food = config.ant_farm_max_food;
    this->ant_queen_food_consumption = config.ant_queen_food_consumption;
    this->ant_food_consumption = config.ant_food_consumption;
    this->nb_food_ant_creation = config.nb_food_ant_creation;
    this->nb_eggs_max_per_iteration = config.nb_eggs_max_per_iteration;
    this->incubation_time = config.incubation_time;
    this->larval_stage_duration = config.larval_stage_duration;
    this->worker_stage_duration = config.worker_stage_duration;
    this->nb_worker_ant_init = config.nb_worker_ant_init;
    this->nb_warrior_ant_init = config.nb_warrior_ant_init;
    this->food_source_max_quantity = config.food_source_max_quantity;
    this->warrior_max_food = config.warrior_max_food;
}


Config &Config::operator=(const Config &config) {
    this->map_height = config.map_height;
    this->map_width = config.map_width;
    this->nb_obstacle = config.nb_obstacle;
    this->nb_source_food = config.nb_source_food;
    this->pheromone_evaporation_rate = config.pheromone_evaporation_rate;
    this->ant_max_life = config.ant_max_life;
    this->ant_max_age = config.ant_max_age;
    this->ant_farm_max_population = config.ant_farm_max_population;
    this->ant_farm_max_food = config.ant_farm_max_food;
    this->ant_queen_food_consumption = config.ant_queen_food_consumption;
    this->ant_food_consumption = config.ant_food_consumption;
    this->nb_food_ant_creation = config.nb_food_ant_creation;
    this->nb_eggs_max_per_iteration = config.nb_eggs_max_per_iteration;
    this->incubation_time = config.incubation_time;
    this->larval_stage_duration = config.larval_stage_duration;
    this->worker_stage_duration = config.worker_stage_duration;
    this->nb_worker_ant_init = config.nb_worker_ant_init;
    this->nb_warrior_ant_init = config.nb_warrior_ant_init;
    this->food_source_max_quantity = config.food_source_max_quantity;
    this->warrior_max_food = config.warrior_max_food;
    return *this;
}
