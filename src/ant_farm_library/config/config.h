#ifndef CONFIG_H
#define CONFIG_H


class Config
{
public:

    Config();
    Config(const Config &config);

    Config &operator=(const Config &config);

    int map_height;
    int map_width;
    int nb_obstacle;
    int nb_source_food;
    int pheromone_evaporation_rate;
    int ant_max_life;
    int ant_max_age;
    int ant_farm_max_population;
    int ant_farm_max_food;
    int ant_queen_food_consumption;
    int ant_food_consumption;
    int nb_food_ant_creation;
    int nb_eggs_max_per_iteration;
    int incubation_time;
    int larval_stage_duration;
    int worker_stage_duration;
    int nb_worker_ant_init;
    int nb_warrior_ant_init;
    int food_source_max_quantity;
    int warrior_max_food;
};

#endif // CONFIG_H
