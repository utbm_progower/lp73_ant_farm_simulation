#include <iostream>
#include <vector>

#include "mainprogram/mainprogram.h"


using namespace std;


int NB_ITERATE = 10;


int main()
{
    cout << "Hello Ghost !" << endl;

    Config config = Config();

    MainProgram mainProgram = MainProgram(config);

    for (int i = 0; i < NB_ITERATE; i++) {
        cout << "Iteration " << i + 1 << " :" << endl;
        mainProgram.iterate();

        cout << "Map :" << endl;
        mainProgram.displayMap();

        cout << "Map Pheromone :" << endl;
        // mainProgram.displayPheromoneMap();

        cout << "Food :" << endl;
        // mainProgram.displayFood();

        cout << "Ant Farm :" << endl;
        mainProgram.displayAntFarm();

        cout << "Warrior Ant :" << endl;
        mainProgram.displayWarriorAnt();
    }

    exit(0);
}
