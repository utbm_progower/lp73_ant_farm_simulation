#ifndef COORDTEST_H
#define COORDTEST_H

#include <QObject>
#include <QtTest>

#include "coord/coord.h"


class CoordTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();
        void test_002();

    public:
        CoordTest();
        ~CoordTest();
};

#endif // COORDTEST_H
