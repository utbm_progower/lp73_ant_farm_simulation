#include "coordtest.h"


CoordTest::CoordTest() {}

CoordTest::~CoordTest() {}


void CoordTest::test_001()
{
    Coord coord = Coord(5, 4);

    QCOMPARE(coord.getX(), 5);
    QCOMPARE(coord.getY(), 4);
}

void CoordTest::test_002()
{
    Coord coord = Coord(5, 4);

    coord.setX(1);
    coord.setY(2);

    QCOMPARE(coord.getX(), 1);
    QCOMPARE(coord.getY(), 2);
}
