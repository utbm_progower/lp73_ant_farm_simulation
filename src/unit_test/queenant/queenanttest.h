#ifndef QUEENANTTEST_H
#define QUEENANTTEST_H

#include <QObject>
#include <QtTest>

#include "queenant/queenant.h"
#include "ant/ant.h"
#include "coord/coord.h"


class QueenAntTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();

    public:
        QueenAntTest();
        ~QueenAntTest();
};

#endif // QUEENANTTEST_H
