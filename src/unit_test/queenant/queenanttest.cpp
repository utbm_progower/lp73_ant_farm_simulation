#include "queenanttest.h"

QueenAntTest::QueenAntTest() {}

QueenAntTest::~QueenAntTest() {}


void QueenAntTest::test_001()
{
    Ant ant = Ant(Coord(5, 7), 100, 1000);
    QueenAnt queenAnt = QueenAnt(ant);

    QCOMPARE(queenAnt.getMaxLife(), 100);
    QCOMPARE(queenAnt.getMaxAge(), 1000);
}
