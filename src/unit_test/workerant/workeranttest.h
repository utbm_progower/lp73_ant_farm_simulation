#ifndef WORKERANTTEST_H
#define WORKERANTTEST_H

#include <QObject>
#include <QtTest>

#include "workerant/workerant.h"
#include "ant/ant.h"
#include "coord/coord.h"


class WorkerAntTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();

    public:
        WorkerAntTest();
        ~WorkerAntTest();
};

#endif // WORKERANTTEST_H
