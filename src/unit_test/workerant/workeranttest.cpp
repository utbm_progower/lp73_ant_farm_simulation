#include "workeranttest.h"


WorkerAntTest::WorkerAntTest() {}

WorkerAntTest::~WorkerAntTest() {}


void WorkerAntTest::test_001()
{
    Ant ant = Ant(Coord(5, 8), 100, 1000);
    WorkerAnt workerAnt = WorkerAnt(ant);

    QCOMPARE(workerAnt.getMaxLife(), 100);
    QCOMPARE(workerAnt.getMaxAge(), 1000);
}
