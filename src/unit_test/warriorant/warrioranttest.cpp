#include "warrioranttest.h"


WarriorAntTest::WarriorAntTest() {}

WarriorAntTest::~WarriorAntTest() {}


void WarriorAntTest::test_001()
{
    Ant ant = Ant(Coord(5, 8), 100, 1000);
    WarriorAnt warriorAnt = WarriorAnt(ant);

    QCOMPARE(warriorAnt.getMaxLife(), 100);
    QCOMPARE(warriorAnt.getMaxAge(), 1000);
    QCOMPARE(warriorAnt.getFoodQuantity(), 0);
    warriorAnt.setFoodQuantity(100);
    QCOMPARE(warriorAnt.getFoodQuantity(), 100);
}
