#ifndef WARRIORANTTEST_H
#define WARRIORANTTEST_H

#include <QObject>
#include <QtTest>

#include "warriorant/warriorant.h"
#include "ant/ant.h"
#include "coord/coord.h"


class WarriorAntTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();

    public:
        WarriorAntTest();
        ~WarriorAntTest();
};

#endif // WARRIORANTTEST_H
