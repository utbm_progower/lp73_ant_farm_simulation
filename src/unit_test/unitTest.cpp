#include <QtTest>
#include <vector>
#include <iostream>

#include "coord/coordtest.h"
#include "element/elementtest.h"
#include "obstacle/obstacletest.h"
#include "pheromone/pheromonetest.h"
#include "ant/anttest.h"
#include "food/foodtest.h"
#include "queenant/queenanttest.h"
#include "workerant/workeranttest.h"
#include "growingant/growinganttest.h"
#include "warriorant/warrioranttest.h"


using namespace std;


int main() {
    int status = 0;
    int tmpStatus = 0;
    vector<string> testList = vector<string>();

    {
        CoordTest coordTest;
        tmpStatus = QTest::qExec(&coordTest);
        if (tmpStatus != 0)
            testList.push_back("Coord Test Error\n");
        else
            testList.push_back("Coord Test Complete\n");
        status |= tmpStatus;
    }
    {
        ElementTest elementTest;
        tmpStatus = QTest::qExec(&elementTest);
        if (tmpStatus != 0)
            testList.push_back("Element Test Error\n");
        else
            testList.push_back("Element Test Complete\n");
        status |= tmpStatus;
    }
    {
        ObstacleTest obstacleTest;
        tmpStatus = QTest::qExec(&obstacleTest);
        if (tmpStatus != 0)
            testList.push_back("Obstacle Test Error\n");
        else
            testList.push_back("Obstacle Test Complete\n");
        status |= tmpStatus;
    }
    {
        PheromoneTest pheromoneTest;
        tmpStatus = QTest::qExec(&pheromoneTest);
        if (tmpStatus != 0)
            testList.push_back("Pheromone Test Error\n");
        else
            testList.push_back("Pheromone Test Complete\n");
        status |= tmpStatus;
    }
    {
        FoodTest foodTest;
        tmpStatus = QTest::qExec(&foodTest);
        if (tmpStatus != 0)
            testList.push_back("Food Test Error\n");
        else
            testList.push_back("Food Test Complete\n");
        status |= tmpStatus;
    }
    {
        AntTest antTest;
        tmpStatus = QTest::qExec(&antTest);
        if (tmpStatus != 0)
            testList.push_back("Ant Test Error\n");
        else
            testList.push_back("Ant Test Complete\n");
        status |= tmpStatus;
    }
    {
        QueenAntTest queenAntTest;
        tmpStatus = QTest::qExec(&queenAntTest);
        if (tmpStatus != 0)
            testList.push_back("QueenAnt Test Error\n");
        else
            testList.push_back("QueenAnt Test Complete\n");
        status |= tmpStatus;
    }
    {
        WorkerAntTest workerAntTest;
        tmpStatus = QTest::qExec(&workerAntTest);
        if (tmpStatus != 0)
            testList.push_back("WorkerAnt Test Error\n");
        else
            testList.push_back("WorkerAnt Test Complete\n");
        status |= tmpStatus;
    }
    {
        GrowingAntTest growingAntTest;
        tmpStatus = QTest::qExec(&growingAntTest);
        if (tmpStatus != 0)
            testList.push_back("GrowingAnt Test Error\n");
        else
            testList.push_back("GrowingAnt Test Complete\n");
        status |= tmpStatus;
    }
    {
        WarriorAntTest warriorAntTest;
        tmpStatus = QTest::qExec(&warriorAntTest);
        if (tmpStatus != 0)
            testList.push_back("WarriorAnt Test Error\n");
        else
            testList.push_back("WarriorAnt Test Complete\n");
        status |= tmpStatus;
    }

    for (int i = 0; i < int(testList.size()); i++)
        cout << testList[i];

    return status;
}
