#include "obstacletest.h"

ObstacleTest::ObstacleTest() {}

ObstacleTest::~ObstacleTest() {}


void ObstacleTest::test_001()
{
    Coord coord = Coord(5, 6);
    Obstacle obstacle = Obstacle(coord);
    QCOMPARE(obstacle.getX(), 5);
    QCOMPARE(obstacle.getY(), 6);
}
