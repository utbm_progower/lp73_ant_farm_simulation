#ifndef OBSTACLETEST_H
#define OBSTACLETEST_H

#include <QObject>
#include <QtTest>

#include "obstacle/obstacle.h"
#include "element/element.h"


class ObstacleTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();

    public:
        ObstacleTest();
        ~ObstacleTest();
};

#endif // ELEMENTTEST_H
