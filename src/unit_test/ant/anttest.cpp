#include "anttest.h"


AntTest::AntTest() {}

AntTest::~AntTest() {}


void AntTest::test_001()
{
    Coord coord = Coord(1, 4);
    Ant ant = Ant(coord, 2, 1);

    QCOMPARE(ant.getLife(), 2);
    ant.incrementLife();
    QCOMPARE(ant.getLife(), 2);
    ant.decrementLife();
    QCOMPARE(ant.getLife(), 1);
    ant.decrementLife();
    QCOMPARE(ant.getLife(), 0);
    ant.decrementLife();
    QCOMPARE(ant.getLife(), 0);
}

void AntTest::test_002()
{
    Coord coord = Coord(2, 8);
    Ant ant = Ant(coord, 1, 1);
    QCOMPARE(ant.getMaxLife(), 1);
    QCOMPARE(ant.getAge(), 0);
    ant.incrementAge();
    QCOMPARE(ant.getAge(), 1);
    ant.incrementAge();
    QCOMPARE(ant.getAge(), 1);
}

void AntTest::test_003()
{
    Coord coord = Coord(6, 9);
    Ant ant = Ant(coord, 1, 1);
    Ant newAnt = Ant(ant);
    newAnt.incrementAge();
    QCOMPARE(newAnt.getAge(), 1);
}

void AntTest::test_004()
{
    Coord coord = Coord(6, 9);
    Ant ant = Ant(coord, 1, 1);
    Ant newAnt = Ant(ant, "newAnt");
    newAnt.incrementAge();
    QCOMPARE(newAnt.getAge(), 1);
    QVERIFY(newAnt.getType() == "newAnt");
}
