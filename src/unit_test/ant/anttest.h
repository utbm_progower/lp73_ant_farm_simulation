#ifndef ANTTEST_H
#define ANTTEST_H

#include <QObject>
#include <QtTest>

#include "ant/ant.h"
#include "element/element.h"

class AntTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();
        void test_002();
        void test_003();
        void test_004();

    public:
        AntTest();
        ~AntTest();
};

#endif // ANTTEST_H
