#include "foodtest.h"

FoodTest::FoodTest() {}

FoodTest::~FoodTest() {}


void FoodTest::test_001()
{
    Coord coord = Coord(7, 8);
    Food food = Food(coord, 10);
    QCOMPARE(food.getQuantity(), 10);
    food.decrementFood();
    QCOMPARE(food.getQuantity(), 9);
}

void FoodTest::test_002()
{
    Coord coord = Coord(7, 8);
    Food food = Food(coord, 1);
    QCOMPARE(food.getQuantity(), 1);
    food.decrementFood();
    QCOMPARE(food.getQuantity(), 0);
    food.decrementFood();
    QCOMPARE(food.getQuantity(), 0);
}

