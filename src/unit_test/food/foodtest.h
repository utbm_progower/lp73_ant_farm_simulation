#ifndef FOODTEST_H
#define FOODTEST_H

#include <QObject>
#include <QtTest>

#include "food/food.h"
#include "element/element.h"


class FoodTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();
        void test_002();

    public:
        FoodTest();
        ~FoodTest();
};

#endif // FOODTEST_H
