#ifndef PHEROMONETEST_H
#define PHEROMONETEST_H

#include <QObject>
#include <QtTest>

#include "pheromone/pheromone.h"
#include "element/element.h"

class PheromoneTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();

    public:
        PheromoneTest();
        ~PheromoneTest();
  };

#endif // PHEROMONETEST_H
