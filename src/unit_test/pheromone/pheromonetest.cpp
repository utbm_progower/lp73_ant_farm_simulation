#include "pheromonetest.h"

PheromoneTest::PheromoneTest() {}

PheromoneTest::~PheromoneTest() {}


void PheromoneTest::test_001()
{
    Coord coord = Coord(7, 8);
    Pheromone pheromone = Pheromone(coord, 10);
    QCOMPARE(pheromone.getQuantity(), 10);
    pheromone.setQuantity(15);
    QCOMPARE(pheromone.getQuantity(), 15);
}
