#include "elementtest.h"


ElementTest::ElementTest() {}

ElementTest::~ElementTest() {}


void ElementTest::test_001()
{
    Coord coord = Coord(5, 6);
    Element element = Element(coord, "none");
    QVERIFY(element.getType() == "none");
    QCOMPARE(element.getX(), 5);
    QCOMPARE(element.getY(), 6);
}
