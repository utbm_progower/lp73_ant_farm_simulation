#ifndef ELEMENTTEST_H
#define ELEMENTTEST_H

#include <QObject>
#include <QtTest>
#include <iostream>

#include "element/element.h"
#include "coord/coord.h"

using namespace std;

class ElementTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();

    public:
        ElementTest();
        ~ElementTest();
};

#endif // ELEMENTTEST_H
