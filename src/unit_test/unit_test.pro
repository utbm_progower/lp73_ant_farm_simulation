QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  \
    unitTest.cpp \
    ant/anttest.cpp \
    coord/coordtest.cpp \
    element/elementtest.cpp \
    food/foodtest.cpp \
    obstacle/obstacletest.cpp \
    pheromone/pheromonetest.cpp \
    queenant/queenanttest.cpp \
    warriorant/warrioranttest.cpp \
    workerant/workeranttest.cpp \
    growingant/growinganttest.cpp

HEADERS += \
    ant/anttest.h \
    coord/coordtest.h \
    element/elementtest.h \
    food/foodtest.h \
    obstacle/obstacletest.h \
    pheromone/pheromonetest.h \
    queenant/queenanttest.h \
    warriorant/warrioranttest.h \
    workerant/workeranttest.h \
    growingant/growinganttest.h

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../ant_farm_library/release/ -lant_farm_library
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../ant_farm_library/debug/ -lant_farm_library
else:unix: LIBS += -L$$OUT_PWD/../ant_farm_library/ -lant_farm_library

INCLUDEPATH += $$PWD/../ant_farm_library
DEPENDPATH += $$PWD/../ant_farm_library
