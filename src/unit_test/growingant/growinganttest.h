#ifndef GROWINGANTTEST_H
#define GROWINGANTTEST_H

#include <QObject>
#include <QtTest>

#include "growingant/growingant.h"
#include "ant/ant.h"
#include "coord/coord.h"


class GrowingAntTest : public QObject
{
    Q_OBJECT

    private slots:
        void test_001();

    public:
        GrowingAntTest();
        ~GrowingAntTest();
};

#endif // GROWINGANTTEST_H
