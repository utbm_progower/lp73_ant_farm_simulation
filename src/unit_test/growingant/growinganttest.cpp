#include "growinganttest.h"

GrowingAntTest::GrowingAntTest() {}

GrowingAntTest::~GrowingAntTest() {}


void GrowingAntTest::test_001()
{
    Ant ant = Ant(Coord(5, 7), 100, 1000);
    GrowingAnt growingAnt = GrowingAnt(ant);

    QCOMPARE(growingAnt.getMaxLife(), 100);
    QCOMPARE(growingAnt.getMaxAge(), 1000);
    QVERIFY(growingAnt.getState() == "egg");
    growingAnt.upgradeState();
    QVERIFY(growingAnt.getState() == "larva");
    growingAnt.upgradeState();
    QVERIFY(growingAnt.getState() == "larva");
}
